﻿using System.Collections.Generic;
using System.Linq;
using Bdx.Api.Load.Testing.Manager.Objects;

namespace Bdx.Api.Load.Testing.Manager.Services.Stats
{
    public class StatsService
    {
        public static StatsResult GetStats(string apiUrl, List<LoadSummary> jmeterResults)
        {
            var result = new StatsResult();

            result.TotalSuccess = jmeterResults.Count(r => r.Url.Contains(apiUrl) && r.Success);
            result.TotalFail = jmeterResults.Count(r => r.Url.Contains(apiUrl) && !r.Success);
            result.AverageTime = jmeterResults.Where(r => r.Url.Contains(apiUrl)).Sum(r => r.ElapsedTime) / (result.TotalSuccess + result.TotalFail);
            result.TotalBytes = jmeterResults.Where(r => r.Url.Contains(apiUrl)).Sum(r => r.SentBytes);
            result.FailureCall = jmeterResults.Where(r => r.Url.Contains(apiUrl) && !r.Success).GroupBy(g => g.Url)
                .Select(g => new {url = g.Key, total = g.Count()}).ToList().OrderByDescending(r => r.total)
                .FirstOrDefault()?.url;
            result.SlowestUrl = jmeterResults.Where(r => r.Url.Contains(apiUrl)).OrderByDescending(r => r.ElapsedTime).FirstOrDefault()?.Url;
            result.ErrorMessage = jmeterResults.Where(r => r.Url.Contains(apiUrl) && !r.Success && !string.IsNullOrEmpty(r.ResponseMessage)).GroupBy(g => g.ResponseMessage)
                .Select(g => new { response = g.Key, total = g.Count() }).ToList().OrderByDescending(r => r.total)
                .FirstOrDefault()?.response;
            var totalElements = jmeterResults.Count(r => r.Url.Contains(apiUrl));
            result.Median = jmeterResults.Where(r => r.Url.Contains(apiUrl)).OrderBy(r => r.ElapsedTime).ToList()[(totalElements / 2)].ElapsedTime;

            return result;
        }
    }
}
