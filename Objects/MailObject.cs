﻿namespace Bdx.Api.Load.Testing.Manager.Objects
{
    public class MailObject
    {
        public string Server { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
