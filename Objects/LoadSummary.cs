﻿namespace Bdx.Api.Load.Testing.Manager.Objects
{
    public class LoadSummary
    {
        public string ResponseCode { get; set; }

        public bool Success { get; set; }

        public int SentBytes { get; set; }

        public string Url { get; set; }

        public int ElapsedTime { get; set; }

        public string ResponseMessage { get; set; }
    }
}
