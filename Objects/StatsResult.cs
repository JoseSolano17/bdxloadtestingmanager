﻿namespace Bdx.Api.Load.Testing.Manager.Objects
{
    public class StatsResult
    {
        public int TotalSuccess { get; set; }
        public int TotalFail { get; set; }
        public int AverageTime { get; set; }
        public int Median { get; set; }
        public string FailureCall { get; set; }
        public string ErrorMessage { get; set; }
        public int TotalBytes { get; set; }
        public string SlowestUrl { get; set; }
    }
}
