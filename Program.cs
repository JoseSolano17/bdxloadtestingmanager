﻿using System;
using System.IO;
using System.Linq;
using Bdx.Api.Load.Testing.Manager.Objects;
using Bdx.Api.Load.Testing.Manager.Services.Email;
using Bdx.Api.Load.Testing.Manager.Services.Stats;
using Microsoft.Extensions.Configuration;

namespace Bdx.Api.Load.Testing.Manager
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            var fileName = configuration.GetSection("Summary").Value;
            var server = configuration.GetSection("SmtpServer").Value;
            var emails = configuration.GetSection("Emails").Value;

            
            var lines = (from line in File.ReadAllLines(fileName).Skip(1)
                let columns = line.Split(',')
                         where columns.Length == 17
                select new LoadSummary
                {
                    ElapsedTime = Convert.ToInt32(columns[1]),
                    ResponseCode = columns[3],
                    Success = bool.Parse(columns[7]),
                    SentBytes = int.Parse(columns[9]),
                    Url = columns[13],
                    ResponseMessage = columns[4]
                }).ToList();

            var nhsStats = StatsService.GetStats("sprint", lines);
            var strategicStats = StatsService.GetStats("strategic", lines);

            var message = EmailService.GetHtmlString(nhsStats, strategicStats);

            EmailService.GenerateEmail(server,emails, message);

            File.Delete(fileName);
        }
    }
}
